#!/usr/bin/python3

"""
Script in charge of launching jobs.
It also manages the instanciation of notifiers and loggers.
Logs/Notifies errors/success according to arguments.
Various configuration for this script can be modified in config.ini.
"""

import ast
import io
import logging
import pickle
import subprocess
import sys
from contextlib import redirect_stderr, redirect_stdout
from pathlib import Path
from types import SimpleNamespace

from config import config
from user import instanciate_user_obj

LOGGERS_DIR = 'loggers'
NOTIFIERS_DIR = 'notifiers'

logger = logging.Logger('dispatcher logger', level=config.dispatcher_log_level)
handler = logging.FileHandler(
    Path(config.dispatcher_log_file).expanduser(),
    encoding='utf-8'
)
handler.setFormatter(
    logging.Formatter(fmt='%(asctime)s|%(levelname)s|%(message)s')
)
logger.addHandler(handler)

try:
    args = sys.argv[1:]

    command, *command_args = args[:args.index(';;')]
    dispatcher_args = dict()
    if args[-1] != ';;':
        dispatcher_args = ast.literal_eval(
            args[args.index(';;') + 1:][0]
        )

    logger.info('Launching job %s', dispatcher_args.get('name'))

    logger.info(
        'Lauching command %s',
        '%s %s' % (command, ' '.join(command_args))
    )

    if dispatcher_args.get('is_pycronjob', False):
        with open(
            Path(__file__).resolve().parent.joinpath(
                'cronjobdata/%s.pkl' % command
            ),
            'rb',
        ) as f:
            exec_info = pickle.load(f)
        with io.StringIO() as stdout, io.StringIO() as stderr:
            with redirect_stdout(stdout), redirect_stderr(stderr):
                try:
                    exec_info['callable'](
                        *exec_info['args'],
                        **exec_info['kwargs'],
                    )
                except Exception as e:
                    returncode = -1
                    stderr.write('\n%s\n' % repr(e))
                else:
                    returncode = 0

            process = SimpleNamespace(
                stdout=stdout.getvalue(),
                stderr=stderr.getvalue(),
                returncode=returncode,
            )
    else:
        process = subprocess.run(
            '%s %s' % (command, ' '.join(command_args)),
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding='utf-8',
        )

    logger.info('Execution over')

    logger_args = dispatcher_args.get('logger')
    if logger_args:
        logger_args['job_name'] = dispatcher_args.get('name')
        logger.debug('Instantiating process logger')
        p_logger = instanciate_user_obj(LOGGERS_DIR, logger_args)

    notifier_args = dispatcher_args.get('notifier')
    if notifier_args:
        notifier_args['job_name'] = dispatcher_args.get('name')
        logger.debug('Instantiating process notifier')
        p_notifier = instanciate_user_obj('notifiers', notifier_args)

    if process.returncode != 0:
        logger.info('Execution error')
        if dispatcher_args.get('log_errors'):
            logger.debug('Logging error')
            p_logger.log(process.stderr.strip())
        if dispatcher_args.get('notify_errors'):
            logger.debug('Notifying error')
            p_notifier.notify(process.stderr.strip())
    else:
        logger.info('Execution success')
        if dispatcher_args.get('log_success'):
            logger.debug('Logging success')
            p_logger.log(process.stdout.strip())
        if dispatcher_args.get('notify_succes'):
            logger.debug('Notifying success')
            p_notifier.notify(process.stdout.strip())
except Exception:
    logger.exception('Error during dispatch')
    sys.exit(-1)

sys.exit(0)
