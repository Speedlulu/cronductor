#!/usr/bin/env python3

import re
import subprocess

from cronjob import CronJob, PyCronJob

__all__ = ('CronManager', )


class CronManager():
    """
    Manager for CronJob of a user.
    If no user is supplied manage current users' jobs.
    Does not work with user defined jobs (written directly in file).
    Only manages jobs created by a manager instance (under
    MANAGE_ENTRIES_SEPERATOR in file).
    All markers need to be unique, they serve as a kind of primary key.
    """

    MANAGED_ENTRIES_SEPARATOR = '# MANAGED ENTRIES UNDER DO NOT EDIT'
    ENTRY_SEPARATOR = '#====='
    MARKER_PATTERN = 'AUTO_MARKER_MJOB_%d'
    MARKER_REGEX = re.compile(r'^AUTO_MARKER_MJOB_(?P<num>\d+)$')

    def __init__(self, user=None):
        if user is not None:
            self._command = 'crontab -u %s' % user
        else:
            self._command = 'crontab'
        self._header = str()
        self.managed_jobs = list()
        self._read_managed_jobs()

    def _get_crontab_text(self):
        process = subprocess.run(
            '%s -l' % self._command,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding='utf-8',
        )

        if process.returncode != 0:
            raise RuntimeError(process.stderr.rstrip())

        return process.stdout.rstrip()

    def _compute_marker(self):
        max_int_marker = 0
        for job in self.managed_jobs:
            match = re.fullmatch(CronManager.MARKER_REGEX, job._marker)
            if match:
                max_int_marker = max(
                    max_int_marker,
                    int(match.group('num'))
                ) + 1
        return CronManager.MARKER_PATTERN % max_int_marker

    def _read_managed_jobs(self):
        crontab_contents = self._get_crontab_text()
        num_added = 0
        if self.MANAGED_ENTRIES_SEPARATOR not in crontab_contents.splitlines():
            self._header = crontab_contents.rstrip()
        else:
            self._header, entries_text = map(
                str.rstrip,
                crontab_contents.split(
                    self.MANAGED_ENTRIES_SEPARATOR
                )
            )
            for cronline in entries_text.split(CronManager.ENTRY_SEPARATOR):
                if cronline:
                    num_added += 1
                    kwargs = CronJob.get_kwargs_from_cronline(
                        cronline.strip()
                    )
                    if kwargs.get('is_pycronjob', False):
                        self.managed_jobs.append(PyCronJob(**kwargs))
                    else:
                        self.managed_jobs.append(CronJob(**kwargs))


        return num_added

    def add_managed_job(self, command, marker=None, **kwargs):
        """Add job to list of managed jobs"""
        marker_list = list()
        for job in self.managed_jobs:
            marker_list.append(job._marker)

        if marker is None:
            marker = self._compute_marker()
        elif callable(marker):
            marker = marker(marker_list)
        else:
            if marker in marker_list:
                raise ValueError('Marker already exists')

        job = CronJob(command, marker, **kwargs)

        self.managed_jobs.append(job)

        return job

    def add_managed_pyjob(self, command, marker=None, **kwargs):
        """Add pyjob to list of managed jobs"""
        marker_list = list()
        for job in self.managed_jobs:
            marker_list.append(job._marker)

        if marker is None:
            marker = self._compute_marker()
        elif callable(marker):
            marker = marker(marker_list)
        else:
            if marker in marker_list:
                raise ValueError('Marker already exists')

        job = PyCronJob(command, marker, **kwargs)

        self.managed_jobs.append(job)

        return job

    def write_managed_jobs(self):
        """Write changes to file"""
        built_file_contents = str()
        built_file_contents += self._header
        if self.managed_jobs:
            built_file_contents += '\n' + CronManager.MANAGED_ENTRIES_SEPARATOR
        for job in self.managed_jobs:
            built_file_contents += '\n' + str(job) + '\n'
            built_file_contents += CronManager.ENTRY_SEPARATOR
        built_file_contents += '\n'

        process = subprocess.run(
            '%s -' % self._command,
            encoding='utf-8',
            shell=True,
            input=built_file_contents,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )

        if process.returncode != 0:
            raise RuntimeError(process.stderr.rstrip())

        return len(self.managed_jobs)

    def get_managed_job(self, name=None, marker=None):
        """
        Get job from marker or name.
        If both arguments are supplied marker has priority.
        First match in list is returned.
        """
        for job in self.managed_jobs:
            if marker and job._marker == marker:
                return job
            elif name and job.name == name:
                return job

    def remove_managed_job(self, name=None, marker=None):
        """
        Remove job from list of managed jobs.
        If both arguments are supplied marker has priority.
        First match in list is returned.
        """
        for job in self.managed_jobs:
            if marker and job._marker == marker:
                self.managed_jobs.remove(job)
                return job
            elif name and job.name == name:
                self.managed_jobs.remove(job)
                return job

    def clear_managed_jobs(self):
        """
        Remove all managed jobs from file
        """
        built_file_contents = str()
        built_file_contents += self._header
        built_file_contents += '\n'

        process = subprocess.run(
            '%s -' % self._command,
            encoding='utf-8',
            shell=True,
            input=built_file_contents,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )

        if process.returncode != 0:
            raise RuntimeError(process.stderr.rstrip())

        return len(self.managed_jobs)
