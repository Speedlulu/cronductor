"""
Import all classes present in __all__ of all python files in this folder
"""

import glob
import importlib
import inspect
import sys
from os.path import join, isfile, dirname, basename


__all__ = []


modules = glob.glob(join(dirname(__file__), "*.py"))
modules = [
    basename(f)[:-3]
    for f in modules if isfile(f) and not f.endswith('__init__.py')
]

for module in modules:
    _m = importlib.import_module('.%s' % module, basename(dirname(__file__)))
    for _class in dict(inspect.getmembers(_m))['__all__']:
        setattr(sys.modules[__name__], _class, getattr(_m, _class))
        __all__.append(_class)
