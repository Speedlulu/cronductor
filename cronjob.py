#!/usr/bin/env python3

import ast
import inspect
import re
import sys
from enum import Enum

import cloudpickle

__all__ = (
    'JobFrequency', 'CronJob',
    'HourlyCronJob', 'DailyCronJob', 'WeeklyCronJob',
    'MonthlyCronJob', 'YearlyCronJob',
)


class JobFrequency(Enum):
    HOURLY = 0
    DAILY = 1
    WEEKLY = 2
    MONTHLY = 3
    YEARLY = 4

    def __str__(self):
        return self.name.lower()

    def __repr__(self):
        return str(self)

    @staticmethod
    def argparse(s):
        try:
            return JobFrequency[s.upper()]
        except KeyError:
            return s


class CronJob():
    """
    Object describing a managed cron job
    """

    DEFAULT_CRON_STRING = '* * * * *'
    CRON_DISPATCHER_STRING = 'dispatcher'
    DISPATCHER_ARG_SEPARATOR = '";;"'

    @staticmethod
    def get_kwargs_from_cronline(cronline):
        """Load a job from cronline found in file"""
        cron_line_regex = r'%s%s%s' % (
            r'^(?P<disabled>#\s)?(?P<cron_string>.+)',
            r'\t(dispatcher\s)?(?P<command>.+?)',
            r'(\s(";;")\s(?P<dispatcher_args>.+))?\s#\s(?P<marker>.+)$'
        )

        lines = cronline.splitlines()
        kwargs = {}
        if len(lines) > 1:
            kwargs['comment'] = '\n'.join(
                line.strip('# ') for line in lines[:-1]
            )
        cronline = lines[-1]

        match = re.match(cron_line_regex, cronline)

        kwargs['cron_string'] = match.group('cron_string')
        kwargs['command'] = match.group('command')
        kwargs['marker'] = match.group('marker')

        if match.group('disabled'):
            kwargs['enabled'] = False
        dispatcher_args = match.group('dispatcher_args')
        if dispatcher_args:
            kwargs.update(ast.literal_eval(dispatcher_args.strip('"\'')))
        return kwargs

    @classmethod
    def job_from_cronline(cls, cronline):
        return cls(**cls.get_kwargs_from_cronline(cronline))

    def __init__(
        self,
        command,
        marker,
        cron_string=None,
        frequency=None,
        log_errors=False,
        log_success=False,
        notify_errors=False,
        notify_success=False,
        logger=None,
        notifier=None,
        comment=None,
        enabled=True,
        name=None,
    ):
        """
        command <str>: command launched by job
        cron_string <str>: string for job frequency in cron format
            m h dom mon dow
            This arguments has priority over frequency
        frequency <JobFrequency> : frequency of job
        log_errors <bool>: log job errors
        log_success <bool>: log job success
        notify_errors <bool>: notify job errors
        notify_success <bool>: notify job success
        logger <dict>: dict containing all the information to instanciate
            logger object from the loggers/ folder
            Format : {
                'klass': name of logger class,
                ... all args to pass to logger at instanciation
            }
        notifier <dict>: dict containing all the information to instanciate
            notifier object from the notifiers/ folder
            Format : {
                'klass': name of notifier class,
                ... all args to pass to notifier at instanciation
            }
        name <str>: name of job
            it can be used to search for a job or it could be displayed
            by a logger or notifier
        """

        self.command = command
        self.cron_string = cron_string
        self._frequency = frequency
        self.log_errors = log_errors
        self.log_success = log_success
        self.notify_errors = notify_errors
        self.notify_success = notify_success
        self.logger = logger
        self.notifier = notifier
        self._marker = marker
        self.comment = comment
        self.enabled = enabled
        self.name = name

        if (self.log_errors or self.log_success) and not self.logger:
            raise ValueError('No logger supplied')
        if (self.notify_errors or self.notify_success) and not self.notifier:
            raise ValueError('No notifier supplied')
        if self.cron_string is None:
            if self._frequency is None:
                self.cron_string = CronJob.DEFAULT_CRON_STRING
            else:
                self._compute_cronstring_from_frequency()

    def _compute_cronstring_from_frequency(self):
        if self._frequency == JobFrequency.HOURLY:
            self.cron_string = '0 * * * *'
        elif self._frequency == JobFrequency.DAILY:
            self.cron_string = '0 0 * * *'
        elif self._frequency == JobFrequency.WEEKLY:
            # Time week days
            #  0   1   2   3   4   5   6
            # Mon Tue Wed Thu Fri Sat Sun
            # Cron week days
            #  1   2   3   4   5   6   0
            # Mon Tue Wed Thu Fri Sat Sun
            try:
                import babel
            except ModuleNotFoundError:
                cron_first_day_of_week = 1
            else:
                cron_first_day_of_week = (babel.Locale(
                    babel.default_locale()
                ).first_week_day + 1) % 7
            self.cron_string = '0 0 * * %d' % cron_first_day_of_week
        elif self._frequency == JobFrequency.MONTHLY:
            self.cron_string = '0 0 1 * *'
        elif self._frequency == JobFrequency.YEARLY:
            self.cron_string = '0 0 1 1 *'

    def set_frequency(self, frequency):
        self._frequency = frequency
        self._compute_cronstring_from_frequency()

    def __str__(self):
        return_string = str()

        # Build dispatcher args dict by getting all object attributes
        # Then removing attributes that are not dispatcher args
        # Finally we remove all args with defaults values

        dispatcher_args = vars(self).copy()
        args_to_pop = (
            'command', 'cron_string', '_frequency', 'comment',
            'enabled', '_marker', 'params', 'kwparams',
        )
        for arg in args_to_pop:
            dispatcher_args.pop(arg)

        # Build init args from all inits
        inits_signatures = {}

        for klass in reversed(self.__class__.__mro__[:-1]):
            inits_signatures.update(dict(
                inspect.signature(klass.__init__).parameters
            ))

        inits_signatures.pop('args', None)
        inits_signatures.pop('kwargs', None)

        for key, value in dispatcher_args.copy().items():
            if inits_signatures.get(key) is None:
                if not value:
                    dispatcher_args.pop(key)
            elif value == inits_signatures[key].default:
                dispatcher_args.pop(key)

        if self.comment is not None:
            for line in self.comment.splitlines():
                return_string += '# %s\n' % line
        if not self.enabled:
            return_string += '# '
        return_string += self.cron_string + '\t'

        # Only launch dispatcher if there are args to pass to it
        if dispatcher_args:
            return_string += CronJob.CRON_DISPATCHER_STRING + ' '

        return_string += self.command

        if dispatcher_args:
            return_string += ' %s "%s"' % (
                CronJob.DISPATCHER_ARG_SEPARATOR,
                str(dispatcher_args)
            )

        if self._marker is not None:
            return_string += ' # %s' % self._marker

        return return_string


class PyCronJob(CronJob):
    PICKLE_IDENTIFIER_PATTERN = 'PKL_%s'
    PICKLE_IDENTIFIER_REGEX = re.compile(r'(?P<prefix>PKL_).+')

    def _build_command(self, command, marker, params=None, kwparams=None):
        self.is_pycronjob = False
        self.params = params
        self.kwparams = kwparams
        if isinstance(command, str):
            if self.PICKLE_IDENTIFIER_REGEX.match(command):
                # Load pickled objects
                job_pkl_identifier = self.PICKLE_IDENTIFIER_PATTERN % marker
                with open(
                    'cronjobdata/%s.pkl' % job_pkl_identifier,
                    'rb'
                ) as f:
                    exec_dict = cloudpickle.load(f)
                command = exec_dict['callable']
                self.params = exec_dict['args']
                self.kwparams = exec_dict['kwargs']
            else:
                try:
                    ast.parse(command)
                except SyntaxError as e:
                    raise ValueError('Invalid python code: %s' % command, e)
                else:
                    command = '%s -c "%s"' % (sys.executable, command)
        elif callable(command):
            self.is_pycronjob = True
            job_pkl_identifier = self.PICKLE_IDENTIFIER_PATTERN % marker
            pickle_dict = {
                'callable': command,
                'args': params,
                'kwargs': kwparams
            }
            with open('cronjobdata/%s.pkl' % job_pkl_identifier, 'wb') as f:
                cloudpickle.dump(pickle_dict, f)
            command = job_pkl_identifier
        else:
            raise ValueError('Could not understand command: ', command)
        return command

    def __init__(
        self,
        command,
        marker,
        *args,
        is_pycronjob=False,
        params=None,
        kwparams=None,
        **kwargs
    ):
        self.is_pycronjob = is_pycronjob
        command = self._build_command(command, marker, params, kwparams)
        super(PyCronJob, self).__init__(command, marker, *args, **kwargs)

    def set_command(self, command, params=None, kwparams=None):
        self.command = self._build_command(
            command,
            self._marker,
            params,
            kwparams
        )




class _HelperCronJobClass(CronJob):
    """Instanciate CronJob with frequency based on name"""
    def __init__(self, *args, **kwargs):
        # Pop frequency to be safe
        kwargs.pop('frequency', None)

        # We get the first class after current class (_HelperCronJobClass)
        # It is either CronJob or PyCronJob
        # Then we remove it from the name of the bottom level class to get
        # frequency

        child_class_mro = self.__class__.__mro__

        frequency_from_name = self.__class__.__name__.replace(
            child_class_mro[
                child_class_mro.index(_HelperCronJobClass) + 1
            ].__name__,
            ''
        ).upper()

        try:
            frequency = JobFrequency[frequency_from_name]
        except KeyError:
            raise ValueError(
                '%s is not a valid frequency name' % frequency_from_name
            )

        super(_HelperCronJobClass, self).__init__(
            frequency=frequency,
            *args,
            **kwargs,
        )


class _HelperPyCronJobClass(_HelperCronJobClass, PyCronJob):
    """Instanciate PyCronJob with frequency based on name"""
    pass


class DailyCronJob(_HelperCronJobClass):
    pass


class WeeklyCronJob(_HelperCronJobClass):
    pass


class MonthlyCronJob(_HelperCronJobClass):
    pass


class YearlyCronJob(_HelperCronJobClass):
    pass


class HourlyCronJob(_HelperCronJobClass):
    pass


class DailyPyCronJob(_HelperPyCronJobClass):
    pass


class WeeklyPyCronJob(_HelperPyCronJobClass):
    pass


class MonthlyPyCronJob(_HelperPyCronJobClass):
    pass


class YearlyPyCronJob(_HelperPyCronJobClass):
    pass


class HourlyPyCronJob(_HelperPyCronJobClass):
    pass
