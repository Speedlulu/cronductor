# ![](https://lucasnetwork.fr/static/images/cronductor_logo_32.png) Cronductor

Cronductor is a utility to manage and retrieve feedback from cron jobs.
It uses the classic Unix utility crontab on the lower level to implement jobs.

It exposes a Python API so you can manage jobs in your scripts.
It also bundles a CLI under the name **cron-manager**.

It also lets you write your own loggers and notifiers (in Python), no loggers and notifiers are bundled with this package, this may change in the future.

The only external dependency is babel, it is optional.
Babel is used for locale dependent first day of week, if it's not installed first day of the week will always be Monday.

Developed on Debian 10 (Buster) with official repositories packages:
Python 3.73, Babel 2.6.0

## Table of Contents

[[_TOC_]]

## Python API

Private methods will not be documented.

### manager.py

#### CronManager

Manager for CronJob of a user.
Does not work with user defined jobs (written directly in file).
Only manages jobs created by a manager instance (under MANAGE_ENTRIES_SEPERATORin file).
All markers need to be unique, they serve as a kind of primary key.

```python
__init__(self, user=None)
```
* user \<str\> if not specified the instance will manage jobs for the current user.

```python
add_managed_job(self, command, marker=None, **kwargs)
```
Add job to list of managed jobs

* command \<str\> command and args of job
* marker \<str\>/\<callable\> marker of job, this methods checks if the marker is unique, if not it fails.
    * \<str\> stored as is
    * \<callable\> needs to return a marker, is passed the list of markers of all managed jobs.
* kwargs \<dict\> args to pass to the `CronJob` constructor.

Returns \<CronJob\> job created

```python
write_managed_jobs(self)
```
Write changes to file.
If this methods is not called, any changes made (adding/removing job, modifying job) will not be effective.

Returns \<int\> number of jobs written to file.

```python
get_managed_job(self, name=None, marker=None)
```
Get job from marker or name.
Scan lines from top to bottom to find a match.
First match in list is returned.
If both arguments are supplied marker has priority.

* name \<str\> name of job
* marker \<str\> marker of job

Returns \<CronJob\> if job found or `None`

```python
remove_managed_job(name=None, marker)
```
Remove job from marker or name.
Scan lines from top to bottom to find a match.
First match in list is returned.
If both arguments are supplied marker has priority.

* name \<str\> name of job
* marker \<str\> marker of job

Returns \<CronJob\> if job removed or `None`

```python
clear_managed_jobs(self)
```
Remove all managed jobs from file.

Returns \<int\> number of managed jobs in `CronManager` instance

### cronjob.py

#### JobFrequency

Defines the "classic" frequencies for a cronjob

Enum :
* HOURLY (every hour at minute 0)
* DAILY (every day at 00:00)
* WEEKLY (on first day of the week, according to locale if Babel is installed, Monday if not, at 00:00)
* MONTHLY (first day of the month at 00:00)
* YEARLY (first day of the year at 00:00)

If you want a job to be executed every minute don't use this enum, the default cron time string for a `CronJob` is `* * * * *`.

#### CronJob

Object describing a managed cron job.
All attributes are safe to access directly for reading/writing except _marker, and _frequency which are only safe when read.
There is a setter for _frequency but not _marker.

There are also five class `HourlyCronJob`, `DailyCronJob`, `WeeklyCronJob`, `MonthlyCronJob`, `YearlyCronJob` which instanciate a CronJob object with the corresponding frequency.

```python
job_from_cronline(cls, cronline)
```
Class method

* cronline \<str\> job data as found in crontab file (comments lines + job line).

Returns a \<CronJob\> instance created with arguments parsed from cronline.

```python
__init__(self, command, cron_string=None, frequency=None, log_errors=False, log_success=False, notify_errors=False, notify_success=False, logger=None, notifier=None, marker=None, comment=None, enabled=True, name=None)
```

* command <str>: command launched by job
* cron_string <str>: string for job frequency.
    * In cron format `m h dom mon dow`.
    * This arguments has priority over frequency.
* frequency <JobFrequency> : frequency of job
* log_errors <bool>: log job errors
* log_success <bool>: log job success
* notify_errors <bool>: notify job errors
* notify_success <bool>: notify job success
* logger <dict>: dict containing all the information to instanciate logger object from the loggers/ folder
    * Format : `
            {
                'klass': name of logger class,
                ... all args to pass to logger at instanciation
            }`
* notifier <dict>: dict containing all the information to instanciate notifier object from the notifiers/ folder
    * Format : `
            {
                'klass': name of notifier class,
                ... all args to pass to notifier at instanciation
            }`
* marker <str>/<callabled>: marker of job
    * If marker is <str> it will be stored as is
    * If marker is <callable> it will be passed a list of markers of currently managed jobs
    * markers have to be unique
* comment <str>: comment to be displayed above job in file
* enabled <bool>: status of job
* name <str>: name of job can be used to search for a job or it could be displayed by a logger or notifier

```python
set_frequency(self, frequency)
```
Sets the frequency for the object and automatically re-generates the corresponding cron_string
* frequency \<JobFrequency\> job frequency

```python
__str__(self)
```
The default `__str__` method is overridden to display the CronJob as it would be written in the crontab file

### Example

```python
from cronjob import JobFrequency
from manager import CronManager

manager = CronManager(user='mroberts')

# Disable a job
job = manager.get_managed_job(marker='AUTO_MARKER_MJOB_0')
job.enabled = False

# Change job frequency
job.set_frequency(JobFrequency.WEEKLY)

# Add job
manager.add_managed_job('echo op', name='Test echo')

# Write changes to file
manager.write_managed_jobs()
```

## Command Line Interface

### General command
All commands directly write to file.
```bash
usage: cron-manager [-h] [-u USER]
                    {add-job,get-job,disable-job,enable-job,remove-job,clear-jobs}
                    ...

positional arguments:
  {add-job,get-job,disable-job,enable-job,remove-job,clear-jobs}
    add-job             Add a CronJob
    get-job             Get a CronJob
    disable-job         Disable a CronJob
    enable-job          Enable a CronJob
    remove-job          Remove a CronJob
    clear-jobs          Clear all CronJobs

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  Cron user
  ```
### add-job command
```bash
usage: cron-manager add-job [-h]
                            [--cron-string CRON_STRING | --frequency {hourly,daily,weekly,monthly,yearly}]
                            [--log-errors] [--log-success] [--notify-errors]
                            [--notify-success] [--logger LOGGER]
                            [--notifier NOTIFIER] [--marker MARKER]
                            [--comment COMMENT] [--disabled] [--name NAME]
                            command

positional arguments:
  command               Command for CronJob

optional arguments:
  -h, --help            show this help message and exit
  --cron-string CRON_STRING
                        Frequency cron string
  --frequency {hourly,daily,weekly,monthly,yearly}
                        CronJob frequency
  --log-errors          CronJob should use logger to log errors
  --log-success         CronJob should use logger to log success
  --notify-errors       CronJob should use notifier to notify success
  --notify-success      CronJob should use notifier to notify errors
  --logger LOGGER       Logger name and args (see doc)
  --notifier NOTIFIER   Notifier name and args (see doc)
  --marker MARKER       CronJob marker
  --comment COMMENT     CronJob comment
  --disabled            Disable job
  --name NAME           CronJob name
```
#### notifier and logger argument
Pass these arguments as a string that will be evaluated to a python dictionary using `json.loads`.
This dictionary should contain the following info:
```python
{
    'klass': <class name of notifier/logger as found in any file in corresponding folder>
}
```

See more in [Loggers and Notifiers](#loggers-and-notifiers)

### get-job, enable-job, disable-job, remove-job

disable/enable commands will return an error if you try to disable/enable a job that is already disabled/enabled.
```bash
usage: cron-manager get-job [-h] (--name NAME | --marker MARKER)

optional arguments:
  -h, --help       show this help message and exit
  --name NAME      Cronjob name
  --marker MARKER  Cronjob marker
```

### clear-jobs
```bash
usage: cron-manager clear-jobs [-h]

optional arguments:
  -h, --help  show this help message and exit
```

## Loggers and Notifiers

You can define your own loggers or notifiers.

All loggers need to be defined in a .py file in the `loggers/` folder. Any logger object needs to define a `log` method, it will be called by the dispatcher when logging is needed and passed a message argument.

Signature for logger:
```python
    class Logger():
        def __init__(self, job_name=None):
            pass

        def log(self, message):
            pass
```


All notifiers need to be defined in a .py file in the `notifiers/` folder. Any notifier object needs to define a `notify` method, it will be called by the dispatcher when notifying is needed and passed a message argument.

Signature for notifier:
```python
    class Notifier():
        def __init__(self, job_name=None):
            pass

        def notify(self, message):
            pass
```

Any number of logger/notifier can be defined in a .py file, but they all need to be declared in `__all__` to be usable.

Here's an example of a notifier:

`notifiers/gnomenotify.py`
```python
import subprocess


__all__ = ('GnomeNotifier', )


class GnomeNotifier():
    def __init__(self, job_name=None, app_name='dispatcher'):
        self.job_name = job_name

    def notify(self, message):
        head = 'Error during %s' % self.job_name if self.job_name else 'Error'

        proc = subprocess.run(
            'notify-send-headless "%s" "%s"' % (head, message),
            shell=True,
            timeout=2,
            stderr=subprocess.PIPE,
        )
        if proc.returncode != 0:
            raise Exception(proc.stderr.decode('utf-8').strip())
```

## Anatomy of a cronline

Here is an exemple of a managed line that you could find your cron file (/var/spool/cron/crontabs/\<user\> or `crontab -l`)

```bash
# MANAGED ENTRIES UNDER DO NOT EDIT
# Comment line 1
# Comment line 2
# * * * * * dispatcher job_command job_command_arg1 job_command_arg2 ";;" "{'log_errors': True, 'log_success': True, 'notify_errors': True, 'notify_success': True, 'logger': {'klass': 'LoggerClass', 'logger_arg1': 1, 'logger_arg2': [1, 2, 'a']}, 'notifier': {'klass': 'NotifierClass', 'notifier_arg1': '1', 'notifier_arg2': {'a': 'b'}}, 'name': 'test_job'}" # AUTO_MARKER_MJOB_0
#=====
```
* All managed lines are under `MANAGED ENTRIES UNDER DO NOT EDIT` edit at your own risk, you can still define your own cron lines above this, they will not be parsed or tampered with.
* All lines can have many lines of comments above them.
* Leading \# show that this job is disabled.
* `* * * * *` is the cron frequency string in the cron format `m h dom mon dow`
* `dispatcher` is the name of the script called to handle all managed jobs.
* Then the command and it's args.
* `";;"`is the separator between the command and dispatcher args, dispatcher args are described in [CronJob init](#cronjob)
* At the end of the line after the final \# is the marker, here it is a marker generated by the manager.
* All managed jobs are separated by `#=====`


