import importlib


__all__ = ('instanciate_user_obj', )


def instanciate_user_obj(folder, obj_args):
    klass_name = obj_args.pop('klass')
    _m = importlib.import_module(folder)
    klass = getattr(_m, klass_name, None)

    if klass is None:
        return klass

    return klass(**obj_args)
